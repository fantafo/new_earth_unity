﻿using System;
using System.Reflection;
using UnityEngine;

[AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
public class DescriptionAttribute : PropertyAttribute
{
    public string description;

    public DescriptionAttribute(string description)
    {
        this.description = description;
    }
}