﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UObject = UnityEngine.Object;
using URandom = UnityEngine.Random;

/// <summary>
/// WaitForSeconds 와 비슷하게 쓸 수 있지만 Condition을 걸어서 사용한다.
/// </summary>
public class WaitForSecondsConditinal : CustomYieldInstruction
{
    float m_endTime;
    Func<bool> m_condition;

    public WaitForSecondsConditinal(float duration, Func<bool> condition)
    {
        m_endTime = Time.time + duration;
        m_condition = condition;
    }

    public override bool keepWaiting
    {
        get
        {
            return !m_condition() || Time.time <= m_endTime;
        }
    }
}
