﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SFramework.ThreadPool
{
    public class SThreadPool
    {
        public SThreadPool()
        {
            MinThreads = 50;
            MaxThreads = 100;
            IsAutoIncrease = false;
        }

        private int minThreads;
        public int MinThreads
        {
            get { return minThreads; }
            set
            {
                minThreads = value;
            }
        }
        public int MaxThreads { get; set; }
        public bool IsAutoIncrease { get; set; }

        Queue<WorkData> waitWorkData = new Queue<WorkData>();
        List<Worker> workingWorkers = new List<Worker>();
        Stack<Worker> sleepWorkers = new Stack<Worker>();


        public bool QueueWorker(Action<object> method, object parameter = null)
        {
            lock (this)
            {
                Worker worker = null;

                // 운용할 워커 데이터 생성
                WorkData data = new WorkData
                {
                    method = method,
                    parameter = parameter
                };

                // 풀에 워커가 남아있다면 그대로 가져다 쓰지만
                // 풀에 워커가 남아있지 않다면 조건에 맞게 새로 생성한다.
                if (sleepWorkers.Count == 0)
                {
                    // 자동증가가 켜져있으면서 워커의 갯수가 최대보다 작다면
                    if (IsAutoIncrease || (workingWorkers.Count + sleepWorkers.Count) <= MaxThreads)
                    {
                        worker = new Worker(this);
                    }
                }
                else
                {
                    worker = sleepWorkers.Pop();
                }

                // 워커를 생성할 수 있다면 해당 워커를 이용하지만
                // 워커를 생성할 수 없다면 다른 워커의 일이 끝날때까지 기다리기위해서
                // 새로운 큐에 저장한다.
                if (worker != null)
                {
                    worker.data = data;
                    workingWorkers.Add(worker);
                    return true;
                }
                else
                {
                    waitWorkData.Enqueue(data);
                    return false;
                }
            }
        }


        private class WorkData
        {
            public Action<object> method;
            public object parameter;
        }

        private class Worker
        {
            static int GEN_ID = 1;

            int ID = ++GEN_ID;

            public SThreadPool parent;
            public WorkData data;
            public bool IsStop;

            public Worker(SThreadPool parent)
            {
                this.parent = parent;
            }

            public void Start()
            {
                var thread = new Thread(Run);
                thread.Name = "SThread-Worker-" + ID;
                thread.Start();
            }
            public void Stop()
            {
                IsStop = true;
            }

            void Run()
            {
                while (!IsStop)
                {
                    if (data != null)
                    {
                        data.method(data.parameter);
                    }
                    if (IsStop)
                    {
                        break;
                    }
                    lock (parent)
                    {
                        if (parent.waitWorkData.Count > 0)
                        {
                            data = parent.waitWorkData.Dequeue();
                            continue;
                        }

                        data = null;
                        parent.sleepWorkers.Push(this);
                        Monitor.Pulse(this);
                    }
                }
                lock (parent)
                {
                    parent.workingWorkers.Remove(this);
                }
            }
        }

    }
}
