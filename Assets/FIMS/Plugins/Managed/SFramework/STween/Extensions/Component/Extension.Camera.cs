﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenCameraExtension
{
    private static readonly Action<Camera, float> _changeFieldOfView = __changeFieldOfView;
    private static void __changeFieldOfView(Camera cam, float val) { cam.fieldOfView = val; }
    public static STweenState twnFieldOfView(this Camera cam, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Camera>, Camera, float>(cam, cam.fieldOfView, to, duration, _changeFieldOfView);
    }
    public static STweenState twnFieldOfView(this Camera cam, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Camera>, Camera, float>(cam, from, to, duration, _changeFieldOfView);
    }

}