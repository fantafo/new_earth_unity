﻿#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using System;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenRectTransformExtension2
{
    private static readonly Action<RectTransform, Vector2> _changeRectTransformAnchored2D = __changeRectTransformAnchored2D;
    private static void __changeRectTransformAnchored2D(RectTransform rectTransform, Vector2 val) { rectTransform.anchoredPosition = val; }
    public static STweenState twnAnchored2D(this RectTransform rectTransform, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<RectTransform>, RectTransform, Vector2>(rectTransform, rectTransform.anchoredPosition, to, duration, _changeRectTransformAnchored2D);
    }
    public static STweenState twnAnchored2D(this RectTransform rectTransform, Vector2 from, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<RectTransform>, RectTransform, Vector2>(rectTransform, from, to, duration, _changeRectTransformAnchored2D);
    }


    private static readonly Action<RectTransform, Vector2> _changeRectTransformAnchorMax = __changeRectTransformAnchorMax;
    private static void __changeRectTransformAnchorMax(RectTransform rectTransform, Vector2 val) { rectTransform.anchorMax = val; }
    public static STweenState twnAnchorMax(this RectTransform rectTransform, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<RectTransform>, RectTransform, Vector2>(rectTransform, rectTransform.anchorMax, to, duration, _changeRectTransformAnchorMax);
    }
    public static STweenState twnAnchorMax(this RectTransform rectTransform, Vector2 from, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<RectTransform>, RectTransform, Vector2>(rectTransform, from, to, duration, _changeRectTransformAnchorMax);
    }


    private static readonly Action<RectTransform, Vector2> _changeRectTransformAnchorMin = __changeRectTransformAnchorMin;
    private static void __changeRectTransformAnchorMin(RectTransform rectTransform, Vector2 val) { rectTransform.anchorMin = val; }
    public static STweenState twnAnchorMin(this RectTransform rectTransform, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<RectTransform>, RectTransform, Vector2>(rectTransform, rectTransform.anchorMin, to, duration, _changeRectTransformAnchorMin);
    }
    public static STweenState twnAnchorMin(this RectTransform rectTransform, Vector2 from, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<RectTransform>, RectTransform, Vector2>(rectTransform, from, to, duration, _changeRectTransformAnchorMin);
    }


    private static readonly Action<RectTransform, Vector2> _changeRectTransformOffsetMax = __changeRectTransformOffsetMax;
    private static void __changeRectTransformOffsetMax(RectTransform rectTransform, Vector2 val) { rectTransform.offsetMax = val; }
    public static STweenState twnOffsetMax(this RectTransform rectTransform, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<RectTransform>, RectTransform, Vector2>(rectTransform, rectTransform.offsetMax, to, duration, _changeRectTransformOffsetMax);
    }
    public static STweenState twnOffsetMax(this RectTransform rectTransform, Vector2 from, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<RectTransform>, RectTransform, Vector2>(rectTransform, from, to, duration, _changeRectTransformOffsetMax);
    }


    private static readonly Action<RectTransform, Vector2> _changeRectTransformOffsetMin = __changeRectTransformOffsetMin;
    private static void __changeRectTransformOffsetMin(RectTransform rectTransform, Vector2 val) { rectTransform.offsetMin = val; }
    public static STweenState twnOffsetMin(this RectTransform rectTransform, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<RectTransform>, RectTransform, Vector2>(rectTransform, rectTransform.offsetMin, to, duration, _changeRectTransformOffsetMin);
    }
    public static STweenState twnOffsetMin(this RectTransform rectTransform, Vector2 from, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<RectTransform>, RectTransform, Vector2>(rectTransform, from, to, duration, _changeRectTransformOffsetMin);
    }


    private static readonly Action<RectTransform, Vector2> _changeRectTransformPivot = __changeRectTransformPivot;
    private static void __changeRectTransformPivot(RectTransform rectTransform, Vector2 val) { rectTransform.pivot = val; }
    public static STweenState twnPivot(this RectTransform rectTransform, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<RectTransform>, RectTransform, Vector2>(rectTransform, rectTransform.pivot, to, duration, _changeRectTransformPivot);
    }
    public static STweenState twnPivot(this RectTransform rectTransform, Vector2 from, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<RectTransform>, RectTransform, Vector2>(rectTransform, from, to, duration, _changeRectTransformPivot);
    }


    private static readonly Action<RectTransform, Vector2> _changeRectTransformSizeDelta = __changeRectTransformSizeDelta;
    private static void __changeRectTransformSizeDelta(RectTransform rectTransform, Vector2 val) { rectTransform.sizeDelta = val; }
    public static STweenState twnSizeDelta(this RectTransform rectTransform, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<RectTransform>, RectTransform, Vector2>(rectTransform, rectTransform.sizeDelta, to, duration, _changeRectTransformSizeDelta);
    }
    public static STweenState twnSizeDelta(this RectTransform rectTransform, Vector2 from, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<RectTransform>, RectTransform, Vector2>(rectTransform, from, to, duration, _changeRectTransformSizeDelta);
    }


}
#endif