﻿#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class STweenRectTransformExtension
{
    #region only To
    public static STweenState twnSize(this RectTransform trans, Vector2 to, float duration)
    {
        return STween.size(trans, to, duration);
    }
    public static STweenState twnSizeX(this RectTransform trans, float to, float duration)
    {
        return STween.sizeX(trans, to, duration);
    }
    public static STweenState twnSizeY(this RectTransform trans, float to, float duration)
    {
        return STween.sizeY(trans, to, duration);
    }

    public static STweenState twnAnchored(this RectTransform trans, Vector3 to, float duration)
    {
        return STween.anchored(trans, to, duration);
    }
    public static STweenState twnAnchoredX(this RectTransform trans, float to, float duration)
    {
        return STween.anchoredX(trans, to, duration);
    }
    public static STweenState twnAnchoredY(this RectTransform trans, float to, float duration)
    {
        return STween.anchoredY(trans, to, duration);
    }
    public static STweenState twnAnchoredZ(this RectTransform trans, float to, float duration)
    {
        return STween.anchoredZ(trans, to, duration);
    }

    public static STweenState twnAlpha(this RectTransform trans, float to, float duration)
    {
        return STween.alpha(trans, to, duration);
    }
    public static STweenState twnColor(this RectTransform trans, Color to, float duration)
    {
        return STween.color(trans, to, duration);
    }
    #endregion

    #region From, To
    public static STweenState twnSize(this RectTransform trans, Vector2 from, Vector2 to, float duration)
    {
        return STween.size(trans, from, to, duration);
    }
    public static STweenState twnSizeX(this RectTransform trans, float from, float to, float duration)
    {
        return STween.sizeX(trans, from, to, duration);
    }
    public static STweenState twnSizeY(this RectTransform trans, float from, float to, float duration)
    {
        return STween.sizeY(trans, from, to, duration);
    }

    public static STweenState twnAnchored(this RectTransform trans, Vector3 from, Vector3 to, float duration)
    {
        return STween.anchored(trans, from, to, duration);
    }
    public static STweenState twnAnchoredX(this RectTransform trans, float from, float to, float duration)
    {
        return STween.anchoredX(trans, from, to, duration);
    }
    public static STweenState twnAnchoredY(this RectTransform trans, float from, float to, float duration)
    {
        return STween.anchoredY(trans, from, to, duration);
    }
    public static STweenState twnAnchoredZ(this RectTransform trans, float from, float to, float duration)
    {
        return STween.anchoredZ(trans, from, to, duration);
    }
    #endregion
}
#endif