﻿using POpusCodec;
using POpusCodec.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;

namespace FTF.VoIP
{
    public enum Codec
    {
        AudioOpus = 11
    }
    public enum FrameDuration
    {
        Frame2dot5ms = 2500,
        Frame5ms = 5000,
        Frame10ms = 10000,
        Frame20ms = 20000,
        Frame40ms = 40000,
        Frame60ms = 60000
    }
    public struct VoiceInfo
    {
        static public VoiceInfo CreateAudioOpus(SamplingRate samplingRate, int sourceSamplingRate, int channels, FrameDuration frameDurationUs, int bitrate, object userdata = null)
        {
            return new VoiceInfo()
            {
                Codec = Codec.AudioOpus,
                SamplingRate = (int)samplingRate,
                SourceSamplingRate = sourceSamplingRate,
                Channels = channels,
                FrameDurationUs = (int)frameDurationUs,
                Bitrate = bitrate,
                UserData = userdata
            };
        }

        public override string ToString()
        {
            return "c=" + Codec + " f=" + SamplingRate + " ch=" + Channels + " d=" + FrameDurationUs + " s=" + FrameSize + " b=" + Bitrate + " w=" + Width + " h=" + Height + " ud=" + UserData;
        }

        public Codec Codec { get; set; }
        public int SamplingRate { get; set; }
        public int SourceSamplingRate { get; set; }
        public int Channels { get; set; }
        public int FrameDurationUs { get; set; }
        public int Bitrate { get; set; }
        public object UserData { get; set; }

        public int FrameDurationSamples { get { return (int)(this.SamplingRate * (long)this.FrameDurationUs / 1000000); } }
        public int FrameSize { get { return this.FrameDurationSamples * this.Channels; } }
        public int Width { get; set; }
        public int Height { get; set; }
    }
    public class EncoderFloat
    {
        private static readonly ArraySegment<byte> EmptyBuffer = new ArraySegment<byte>(new byte[] { });
        protected OpusEncoder encoder;
        protected bool disposed;

        public EncoderFloat(VoiceInfo i)
        {
            encoder = new OpusEncoder((SamplingRate)i.SamplingRate, (Channels)i.Channels, i.Bitrate, OpusApplicationType.Voip, (Delay)(i.FrameDurationUs * 2 / 1000));
        }

        public ArraySegment<byte> EncodeAndGetOutput(float[] buf)
        {
            lock (this)
            {
                if (disposed)
                    return EmptyBuffer;
                else
                    return encoder.Encode(buf);
            }
        }

        public void Dispose()
        {
            lock (this)
            {
                encoder.Dispose();
                disposed = true;
            }
        }
    }
    public interface IBufferReader<T> : IDisposable
    {
        /// <summary>
        /// Fill full given frame buffer with source unconmressed data or return false if not enough such data.
        /// </summary>
        bool Read(T[] buffer);
    }
    public class Framer<T>
    {
        T[] frame;
        public Framer(int frameSize)
        {
            this.frame = new T[frameSize];
            var x = new T[1];
            if (x[0] is byte)
                this.sizeofT = sizeof(byte);
            else if (x[0] is short)
                this.sizeofT = sizeof(short);
            else if (x[0] is float)
                this.sizeofT = sizeof(float);
            else
                throw new Exception("Input data type is not supported: " + x[0].GetType());

        }
        int sizeofT;
        int framePos = 0;

        public IEnumerable<T[]> Frame(T[] buf)
        {
            var s = frame.Length;
            // quick return in trivial case
            if (s == buf.Length && framePos == 0)
            {
                yield return buf;
            }
            else
            {
                var bufPos = 0;

                while (bufPos + s - framePos <= buf.Length)
                {
                    var l = s - framePos;
                    Buffer.BlockCopy(buf, bufPos * sizeofT, frame, framePos * sizeofT, l * sizeofT);
                    //Console.WriteLine("=== Y {0} {1} -> {2} {3} ", bufPos, bufPos + l, sourceFramePos, sourceFramePos + l);
                    bufPos += l;
                    framePos = 0;

                    yield return this.frame;
                }
                if (bufPos != buf.Length)
                {
                    var l = buf.Length - bufPos;
                    Buffer.BlockCopy(buf, bufPos * sizeofT, frame, framePos * sizeofT, l * sizeofT);
                    //Console.WriteLine("=== L {0} {1} -> {2} {3} ", bufPos, bufPos + l, sourceFramePos, sourceFramePos + l);
                    framePos += l;
                }
            }
        }
    }
    public interface IProcessor<T> : IDisposable
    {
        T[] Process(T[] buf);
    }

    /// <summary>
    /// Audio parameters and data conversion utilities.
    /// </summary>
    public static class AudioUtil
    {

        public static void Resample<T>(T[] src, T[] dst, int dstCount, int channels)
        {
            //TODO: Low-pass filter
            for (int i = 0; i < dstCount; i += channels)
            {
                var interp = (i * src.Length / dstCount);
                for (int ch = 0; ch < channels; ch++)
                {
                    dst[i + ch] = src[interp + ch];
                }
            }
        }

        internal static string tostr<T>(T[] x, int lim = 10)
        {
            System.Text.StringBuilder b = new System.Text.StringBuilder();
            for (var i = 0; i < (x.Length < lim ? x.Length : lim); i++)
            {
                b.Append("-");
                b.Append(x[i]);
            }
            return b.ToString();
        }

        public class Resampler<T> : IProcessor<T>
        {
            protected T[] frameResampled;
            int channels;
            public Resampler(int dstSize, int channels)
            {
                this.frameResampled = new T[dstSize];
                this.channels = channels;
            }
            public T[] Process(T[] buf)
            {
                AudioUtil.Resample(buf, this.frameResampled, this.frameResampled.Length, channels);
                return this.frameResampled;
            }
            public void Dispose()
            {
            }

        }
        public interface ILevelMeter
        {
            /// <summary>
            /// Average of last values in current 1/2 sec. buffer.
            /// </summary>

            float CurrentAvgAmp { get; }

            /// <summary>
            /// Max of last values in 1/2 sec. buffer as it was at last buffer wrap.
            /// </summary>
            float CurrentPeakAmp
            {
                get;
            }

            /// <summary>
            /// Average of CurrentPeakAmp's since last reset.
            /// </summary>
            float AccumAvgPeakAmp { get; }

            /// <summary>
            /// Reset LevelMeter.AccumAvgPeakAmp.
            /// </summary>
            void ResetAccumAvgPeakAmp();
        }

        public class LevelMetterDummy : ILevelMeter
        {
            public float CurrentAvgAmp { get { return 0; } }
            public float CurrentPeakAmp { get { return 0; } }
            public float AccumAvgPeakAmp { get { return 0; } }
            public void ResetAccumAvgPeakAmp() { }
        }
        /// <summary>
        /// Utility for measurement audio signal parameters.
        /// </summary>
        abstract public class LevelMeter<T> : IProcessor<T>, ILevelMeter
        {
            // sum of all values in buffer
            protected float ampSum;
            // max of values from start buffer to current pos
            protected float ampPeak;
            protected int bufferSize;
            protected float[] buffer;
            protected int prevValuesPtr;

            protected float accumAvgPeakAmpSum;
            protected int accumAvgPeakAmpCount;

            internal LevelMeter(int samplingRate, int numChannels)
            {
                this.bufferSize = samplingRate * numChannels / 2; // 1/2 sec
                this.buffer = new float[this.bufferSize];
            }

            public float CurrentAvgAmp { get { return ampSum / this.bufferSize; } }
            public float CurrentPeakAmp
            {
                get;
                protected set;
            }

            public float AccumAvgPeakAmp { get { return this.accumAvgPeakAmpCount == 0 ? 0 : accumAvgPeakAmpSum / this.accumAvgPeakAmpCount; } }

            public void ResetAccumAvgPeakAmp() { this.accumAvgPeakAmpSum = 0; this.accumAvgPeakAmpCount = 0; }

            public abstract T[] Process(T[] buf);

            public void Dispose()
            {
            }
        }

        public class LevelMeterFloat : LevelMeter<float>
        {
            public LevelMeterFloat(int samplingRate, int numChannels) : base(samplingRate, numChannels) { }
            public override float[] Process(float[] buf)
            {
                foreach (var v in buf)
                {
                    var a = v;
                    if (a < 0)
                    {
                        a = -a;
                    }
                    ampSum = ampSum + a - this.buffer[this.prevValuesPtr];
                    this.buffer[this.prevValuesPtr] = a;

                    if (ampPeak < a)
                    {
                        ampPeak = a;
                    }
                    if (this.prevValuesPtr == 0)
                    {
                        CurrentPeakAmp = ampPeak;
                        ampPeak = 0;
                        accumAvgPeakAmpSum += CurrentPeakAmp;
                        accumAvgPeakAmpCount++;
                    }
                    this.prevValuesPtr = (this.prevValuesPtr + 1) % this.bufferSize;
                }
                return buf;
            }
        }

        public class LevelMeterShort : LevelMeter<short>
        {
            public LevelMeterShort(int samplingRate, int numChannels) : base(samplingRate, numChannels) { }
            public override short[] Process(short[] buf)
            {
                foreach (var v in buf)
                {
                    var a = v;
                    if (a < 0)
                    {
                        a = (short)-a;
                    }
                    ampSum = ampSum + a - this.buffer[this.prevValuesPtr];
                    this.buffer[this.prevValuesPtr] = a;

                    if (ampPeak < a)
                    {
                        ampPeak = a;
                    }
                    if (this.prevValuesPtr == 0)
                    {
                        CurrentPeakAmp = ampPeak;
                        ampPeak = 0;
                        accumAvgPeakAmpSum += CurrentPeakAmp;
                        accumAvgPeakAmpCount++;
                    }
                    this.prevValuesPtr = (this.prevValuesPtr + 1) % this.bufferSize;
                }
                return buf;
            }
        }

        public interface IVoiceDetector
        {
            /// <summary>If true, voice detection enabled.</summary>
            bool On { get; set; }
            /// <summary>Voice detected as soon as signal level exceeds threshold.</summary>
            float Threshold { get; set; }

            /// <summary>If true, voice detected.</summary>
            bool Detected { get; }

            /// <summary>Keep detected state during this time after signal level dropped below threshold.</summary>
            int ActivityDelayMs { get; set; }
        }

        public class VoiceDetectorCalibration<T> : IProcessor<T>
        {
            IVoiceDetector voiceDetector;
            ILevelMeter levelMeter;
            int samplesPerSec;
            public bool VoiceDetectorCalibrating { get { return voiceDetectorCalibrateCount > 0; } }
            protected int voiceDetectorCalibrateCount;

            public VoiceDetectorCalibration(IVoiceDetector voiceDetector, ILevelMeter levelMeter, int samplesPerSec)
            {
                this.samplesPerSec = samplesPerSec;
                this.voiceDetector = voiceDetector;
                this.levelMeter = levelMeter;
            }
            public void VoiceDetectorCalibrate(int durationMs)
            {
                this.voiceDetectorCalibrateCount = samplesPerSec * durationMs / 1000;
                levelMeter.ResetAccumAvgPeakAmp();
            }
            public T[] Process(T[] buf)
            {

                if (this.voiceDetectorCalibrateCount != 0)
                {
                    this.voiceDetectorCalibrateCount -= buf.Length;
                    if (this.voiceDetectorCalibrateCount <= 0)
                    {
                        this.voiceDetectorCalibrateCount = 0;
                        this.voiceDetector.Threshold = levelMeter.AccumAvgPeakAmp * 2;
                    }
                }
                return buf;
            }

            public void Dispose()
            {
            }
        }
        public class VoiceDetectorDummy : IVoiceDetector
        {
            public bool On { get { return false; } set { } }
            public float Threshold { get { return 0; } set { } }
            public bool Detected { get { return false; } }
            public int ActivityDelayMs { get { return 0; } set { } }
        }
        /// <summary>
        /// Simple voice activity detector triggered by signal level.
        /// </summary>
        abstract public class VoiceDetector<T> : IProcessor<T>, IVoiceDetector
        {
            public bool On { get; set; }
            public float Threshold { get; set; }
            public bool Detected { get; protected set; }
            public int ActivityDelayMs
            {
                get { return this.activityDelay; }
                set
                {
                    this.activityDelay = value;
                    this.activityDelayValuesCount = value * valuesCountPerSec / 1000;
                }
            }

            protected int activityDelay;
            protected int autoSilenceCounter = 0;
            protected int valuesCountPerSec;
            protected int activityDelayValuesCount;

            internal VoiceDetector(int samplingRate, int numChannels)
            {
                this.valuesCountPerSec = samplingRate * numChannels;
                this.Threshold = 0.01f;
                this.ActivityDelayMs = 500;
            }

            public abstract T[] Process(T[] buf);

            public void Dispose()
            {
            }
        }

        public class VoiceDetectorFloat : VoiceDetector<float>
        {
            public VoiceDetectorFloat(int samplingRate, int numChannels) : base(samplingRate, numChannels) { }
            public override float[] Process(float[] buffer)
            {
                if (this.On)
                {
                    foreach (var s in buffer)
                    {
                        if (s > this.Threshold)
                        {
                            this.Detected = true;
                            this.autoSilenceCounter = 0;
                        }
                        else
                        {
                            this.autoSilenceCounter++;
                        }
                    }
                    if (this.autoSilenceCounter > this.activityDelayValuesCount)
                    {
                        this.Detected = false;
                    }
                    return Detected ? buffer : null;
                }
                else
                {
                    return buffer;
                }
            }
        }

        public class VoiceDetectorShort : VoiceDetector<short>
        {
            internal VoiceDetectorShort(int samplingRate, int numChannels) : base(samplingRate, numChannels) { }
            public override short[] Process(short[] buffer)
            {
                if (this.On)
                {
                    foreach (var s in buffer)
                    {
                        if (s > this.Threshold)
                        {
                            this.Detected = true;
                            this.autoSilenceCounter = 0;
                        }
                        else
                        {
                            this.autoSilenceCounter++;
                        }
                    }
                    if (this.autoSilenceCounter > this.activityDelayValuesCount)
                    {
                        this.Detected = false;
                    }
                    return Detected ? buffer : null;
                }
                else
                {
                    return buffer;
                }
            }
        }
    }
}