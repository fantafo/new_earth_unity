﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

namespace FTF.VoIP
{
    /// <summary>
    /// VoiceSource에 음성 패킷을 전달하기 위한 서버 컴포넌트다.
    /// VoiceSource의 접속을 받아서 유지하고 패킷을 전송하는 역할을 한다.
    /// 
    /// VoIPServer는 한개만 활성화 될 수 있으며, 만약 다른 객체로 교체할때는 
    /// 미리 생성 돼 있는 객체를 비활성화한 뒤 사용한다.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(VoiceRecorder))]
    [AddComponentMenu("Voice/Voice Server")]
    public class VoiceServer : MonoBehaviour, IVoiceSender
    {
        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Statics
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        static VoiceServer main;

        /// <summary>
        /// 서버가 시작됐다는 사실을 알린다.
        /// </summary>
        public static bool IsRun { get; private set; }

        /// <summary>
        /// true일 경우 서버가 활성화(Binding) 된 상태이다.
        /// </summary>
        public static bool IsPending { get; private set; }

        /// <summary>
        /// true일 경우 서버가 활성화 된 상태이다.
        /// </summary>
        public static bool IsAnyone { get; private set; }

        /// <summary>
        /// 현재 디바이스의 Ip주소를 알 수 있다.
        /// </summary>
        public static string ServerIPAddress { get; private set; }

        /// <summary>
        /// 현재 활성화된 디바이스 서버의 Port를 알 수 있다. 0일경우 서버가 활성화되지 않은 상태다.
        /// </summary>
        public static int ServerPort { get; private set; }


        /// <summary>
        /// IP가 변경될 경우 호출되는 Listener
        /// </summary>
        public static event Action OnChangedIPAddress;

        /// <summary>
        /// 서버의 열림상태에 따른 변화를 알려주는 Listener
        /// </summary>
        public static event Action OnChangedBind;

        /// <summary>
        /// 유니티가 시작될때 IP주소와 포트를 초기화한다.
        /// </summary>
        [RuntimeInitializeOnLoadMethod]
        static void Initialize()
        {
            ServerIPAddress = Network.player.ipAddress;
            ServerPort = 0;
        }




        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Member Variables
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        //-- Inspectors
        [Tooltip("기본적으로 사용할 서버 Port를 지정한다. 만약 해당 포트가 사용중이라면 1씩 추가하여 서버를 생성한다. 이 값은 VoIPServer.VoIPPort 와 일치하게된다.")]
        [SerializeField]
        private int _port = 55155;

        //-- Programmatically Private:
        private VoiceRecorder _recorder;
        private List<VoIPClient> clients = new List<VoIPClient>();
        private Socket _listener;
        private Thread _listenerThread;

        private byte[] _sendBuffer;
        private MemoryStream _sendStream;
        private BinaryWriter _sendWriter;

#if UNITY_EDITOR
        [Space]
        [Header("Network Monitor")]
        public int _totalReadPacketSize = 0;
        public int _averageReadPacketSize = 0;
        public DateTime _readStartTime;
#endif


        public bool IsInitialized { get { return IsRun; } }



        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Unity Lifecycles
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        private void Awake()
        {
            _sendBuffer = new byte[2048];
            _sendStream = new MemoryStream(_sendBuffer, true);
            _sendWriter = new BinaryWriter(_sendStream, Encoding.UTF8);

            _recorder = GetComponent<VoiceRecorder>();
        }

        private void OnEnable()
        {
            if (main)
            {
                Debug.LogError("이미 활성화된 VoIPServer가 존재합니다. 해당 서버를 종료한 뒤 활성화 해 주세요.", main);
                this.enabled = false;
                return;
            }

            main = this;
            StartCoroutine(_routine_ValidateIPAddress());
        }

        private void OnDisable()
        {
            if (main == this)
                main = null;

            StopServer();
        }

        /// <summary>
        /// 1초에 한번씩 IP주소가 바뀌었는지 검사한다.
        /// </summary>
        IEnumerator _routine_ValidateIPAddress()
        {
            var wait = new WaitForSecondsRealtime(1);
            while (true)
            {
                ValidateVoIPAddress();
                yield return wait;
            }
        }
        void ValidateVoIPAddress()
        {
            string currentIpAddress = Network.player.ipAddress;
            if (currentIpAddress != ServerIPAddress)
            {
                ServerIPAddress = currentIpAddress;

                if (OnChangedIPAddress != null)
                    OnChangedIPAddress();
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Server Lifecycles
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        public void StartServer()
        {
            if (!IsRun && !IsPending)
            {
                IsPending = true;
                IsAnyone = false;

                IsRun = true;
                _listenerThread = new Thread(_thread_Listener);
                _listenerThread.Name = "[VoIP-Srv]";
                _listenerThread.Start();

                Debug.Log("[Voice] Server Start : " + _port);
            }
        }

        public void StopServer()
        {
            IsRun = false;
            IsPending = false;
            IsAnyone = false;

            DisconnectAllClients();

            // 소켓 종료
            if (_listener != null)
            {
                try
                { _listener.Close(); }
                catch { }
                _listener = null;
            }

            // 쓰레드 종료
            if (_listenerThread != null)
            {
                _listenerThread.Join();
                _listenerThread = null;
            }
        }

        /// <summary>
        /// 접속자들을 강제종료합니다.
        /// </summary>
        public void DisconnectAllClients()
        {
            lock (clients)
            {
                for (int i = 0; i < clients.Count; i++)
                {
                    OnDisconnect(clients[i]);
                }
                clients.Clear();
            }
        }

        public void RestartSender()
        {
            DisconnectAllClients();
        }

        public void StartSender()
        {
            StartServer();
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Threads
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        private void _thread_Listener()
        {
            try
            {
                //-- 서버를 할당한다.
                _bindingListener();

                // 서버가 할당 됐다는 것을 알린다.
                IsPending = true;

                while (IsRun)
                {
                    _waitForClient();
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
        void _bindingListener()
        {
            _listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            for (int addPort = 0, loopLength = UInt16.MaxValue - _port; IsRun && addPort < loopLength; addPort++)
            {
                try
                {
                    var ipEndPoint = new IPEndPoint(IPAddress.Any, _port);
                    Debug.Log("[Voice-Server] Try Bind " + ipEndPoint);

                    _listener.Bind(ipEndPoint);
                    _listener.Listen(100);
                    Debug.Log("[Voice-Server] Binded " + ipEndPoint);
                    break;
                }
                catch (SocketException e)
                {
                    switch (e.SocketErrorCode)
                    {
                        case SocketError.AddressAlreadyInUse:
                            break;
                        default:
                            Debug.LogException(e);
                            break;
                    }
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    return;
                }
                _port++;
            }
        }
        void _waitForClient()
        {
            try
            {
                Socket socket = _listener.Accept();
                VoIPClient client = new VoIPClient(socket);
                OnConnect(client);
            }
            catch (SocketException e)
            {
                if (IsPending)
                {
                    switch (e.ErrorCode)
                    {
                        case 10004: // WSACancelBlockingCall를 호출하여 차단 작업이 중단되었습니다.
                            break;
                        default:
                            Debug.LogException(e);
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                if (IsPending)
                {
                    Debug.LogException(e);
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                               Connection Callback
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        public void OnConnect(VoIPClient client)
        {
            lock (clients)
            {
                _sendStream.SetLength(0);
                _sendWriter.Write((byte)VoiceOpcode.OPCODE_VOICE_INFO);
                _sendWriter.Write((int)_recorder._voiceInfo.SamplingRate);
                _sendWriter.Write((byte)_recorder._voiceInfo.Channels);
                _sendWriter.Write((int)_recorder._voiceInfo.FrameDurationSamples);

                int len = (int)_sendStream.Length;
                sizeBuffer[0] = (byte)(len & 0xFF);
                sizeBuffer[1] = (byte)((len >> 0x08) & 0xFF);

                client.stream.Write(sizeBuffer, 0, 2);
                client.stream.Write(_sendBuffer, 0, len);
                AddSendMonitor(len);

                Debug.Log("[Voice] Accepted " + client.ipAddress);
                clients.Add(client);
                IsAnyone = true;
            }
        }
        public void OnDisconnect(VoIPClient client)
        {
            lock (clients)
            {
                Debug.Log("[Voice] Discconnect " + client.ipAddress);
                clients.Remove(client);
                if (clients.Count == 0)
                    IsAnyone = false;
            }
            client.Disconnect();
        }


        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                               Send Method
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        byte[] sizeBuffer = new byte[2];
        public void Broadcast(byte[] buffer, int offset, int len)
        {
            lock (clients)
            {
                sizeBuffer[0] = (byte)(len & 0xFF);
                sizeBuffer[1] = (byte)((len >> 0x08) & 0xFF);

                for (int i = 0; i < clients.Count; i++)
                {
                    try
                    {
                        clients[i].stream.Write(sizeBuffer, 0, 2);
                        clients[i].stream.Write(buffer, offset, len);
                        clients[i].stream.Flush();
                        AddSendMonitor(len);
                    }
                    catch (IOException e)
                    {
                        var se = (e.InnerException as SocketException);
                        if (se == null || se.ErrorCode != 10054) //현재 연결은 원격 호스트에 의해 강제로 끊겼습니다.
                        {
                            Debug.LogException(e);
                        }
                        OnDisconnect(clients[i--]);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                        OnDisconnect(clients[i--]);
                    }
                }
            }
        }

        public void BroadcastInfo()
        {
            lock (clients)
            {
                _sendStream.SetLength(0);
                _sendWriter.Write((byte)VoiceOpcode.OPCODE_VOICE_INFO);
                _sendWriter.Write((int)_recorder._voiceInfo.SamplingRate);
                _sendWriter.Write((byte)_recorder._voiceInfo.Channels);
                _sendWriter.Write((int)_recorder._voiceInfo.FrameDurationSamples);

                Broadcast(_sendBuffer, 0, (int)_sendStream.Length);
            }
        }

        public class VoIPClient
        {
            public VoIPClient(Socket socket)
            {
                this.socket = socket;
                this.stream = new NetworkStream(socket);
                ipAddress = ((IPEndPoint)socket.RemoteEndPoint).Address.ToString();
            }

            public string ipAddress;
            public Socket socket;
            public NetworkStream stream;

            public void Disconnect()
            {
                try
                { socket.Close(); }
                catch { }
                try
                { stream.Close(); }
                catch { }
                socket = null;
                stream = null;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Editor Network Monitor
        //
        //////////////////////////////////////////////////////////////////////////////////////////
#if UNITY_EDITOR
        [Space]
        [Header("Network Monitor")]
        public int _totalPacketSize = 0;
        public int _averagePacketSize = 0;
        public DateTime _packetStartTime = new DateTime(0);
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        void AddSendMonitor(int size)
        {
            if (_packetStartTime.Ticks == 0)
                _packetStartTime = DateTime.Now;

            _totalPacketSize += size + 2;
            _averagePacketSize = (int)(_totalPacketSize / (DateTime.Now - _packetStartTime).TotalSeconds);
        }
#else
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        void AddSendMonitor(int size){}
#endif
    }
}