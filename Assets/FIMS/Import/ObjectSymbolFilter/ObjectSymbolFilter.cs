﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEditor;
#endif

public class ObjectSymbolFilter : MonoBehaviour
{
    [Tooltip("PlayerSetting.ScriptingDefineSymbols에 등록 돼 있는 symbol을 가리킨다")]
    public string symbol;

    [Tooltip("대상 객체를 제거하고 빌드한다. 대상 객체를 지정하지 않으면 이 스크립트를 가지고있는 gameObject가 대상이 된다.")]
    public Object target;

    [Tooltip("true일 경우, symbol이 등록이 되어 있지 않다면 대상을 삭제한다. false일 경우 등록돼있을 경우에 삭제된다.")]
    public bool invalidDestroy = true;

    [Tooltip("제거되지 않았다면 객체를 active시킨다")]
    public bool notDestroyOnActive = true;

#if UNITY_EDITOR
    private void Awake()
    {
        CheckFilter(GetComponent<ObjectSymbolFilter>(), true);
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    public static void OnInitializeA()
    {
        CheckFilterAll(true);
        SceneManager.sceneLoaded += (a, b) =>
        {
            CheckFilterAll(true);
        };
    }

    [UnityEditor.Callbacks.PostProcessScene(5)]
    public static void OnSceneprocess()
    {
        CheckFilterAll(false);
    }
    public static void CheckFilterAll(bool ingame)
    {
        var scene = SceneManager.GetActiveScene();
        if (scene.isLoaded)
        {
            foreach (var root in scene.GetRootGameObjects())
            {
                foreach (var filter in root.GetComponentsInChildren<ObjectSymbolFilter>(true))
                {
                    CheckFilter(filter, ingame);
                }
            }
        }
    }
    public static void CheckFilter(ObjectSymbolFilter filter, bool ingame)
    {
        if (filter)
        {
            string[] symbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup).Split(';');
            var scene = filter.gameObject.scene;

            if (string.IsNullOrEmpty(filter.symbol))
            {
                Debug.LogWarningFormat(filter.gameObject, "[SymbolFilter] '{0}' Scene의 '{1}'객체의 Filter는 Symbol이 등록돼 있지 않아 제거했습니다.", scene.name, filter.name);
                DestroyImmediate(filter);
            }
            else
            {
                bool invalid = (System.Array.IndexOf(symbols, filter.symbol) == -1);
                if (invalid == filter.invalidDestroy)
                {
                    if (filter.target != null)
                    {
                        Debug.LogFormat(filter.gameObject, "[SymbolFilter] '{0}' Scene의 '{1}'필터에 의해 '{2}({3})'를 제거했습니다.", scene.name, filter.name, filter.target.name, filter.target.GetType().Name);
                        if (ingame)
                        {
                            Destroy(filter.target);
                        }
                        else
                        {
                            DestroyImmediate(filter.target);
                        }
                    }
                    else
                    {
                        Debug.LogFormat(filter.gameObject, "[SymbolFilter] '{0}' Scene의 '{1}(GameObject)'를 제거했습니다.", scene.name, filter.name);
                        if (ingame)
                        {
                            Destroy(filter.gameObject);
                        }
                        else
                        {
                            DestroyImmediate(filter.gameObject);
                        }
                    }
                }
                else
                {
                    if (filter.notDestroyOnActive)
                    {
                        if (filter.target != null)
                        {
                            if (filter.target is GameObject)
                            {
                                Debug.LogFormat(filter.target, "[SymbolFilter] '{0}({1})'을 활성화했습니다.", filter.name, filter.target.GetType().Name);
                                ((GameObject)filter.target).SetActive(true);
                            }
                            else
                            {
                                Debug.LogFormat(((Component)filter.target).gameObject, "[SymbolFilter] '{0}({1})'을 활성화했습니다.", filter.name, filter.target.GetType().Name);
                                ((Component)filter.target).gameObject.SetActive(true);
                            }
                        }
                        else
                        {
                            Debug.LogFormat(filter.gameObject, "[SymbolFilter] '{0}(GameObject)'을 활성화했습니다.", filter.name);
                            filter.gameObject.SetActive(true);
                        }
                    }
                    else
                    {
                        filter.gameObject.SetActive(false);
                        Debug.LogFormat(filter.gameObject, "[SymbolFilter] '{0}(GameObject)'를 비활성화했습니다.", filter.name);
                    }
                    DestroyImmediate(filter);
                }
            }
        }
    }
#endif
}
