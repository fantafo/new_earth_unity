﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace FTF
{
    /// <summary>
    /// 네트워크를 이용할때 쓰레드별로 BinaryWriter를 매번 새로 생성하는 것이 아닌, 
    /// 생성돼 있는 객체들을 가져와서 사용하도록합니다.
    /// </summary>
    public static class ThreadBaseBinaryWriter
    {
        static Dictionary<Thread, BinaryWriter> streams = new Dictionary<Thread, BinaryWriter>();
        public static BinaryWriter GetWriter()
        {
            BinaryWriter writer = null;
            if (!streams.TryGetValue(Thread.CurrentThread, out writer))
            {
                streams.Add(Thread.CurrentThread, writer = new BinaryWriter(new MemoryStream(1024), Encoding.UTF8));
            }
            else
            {
                ((MemoryStream)writer.BaseStream).SetLength(0);
            }
            return writer;
        }

        public static byte[] ToBytes(this BinaryWriter bw)
        {
            MemoryStream ms = bw.BaseStream as MemoryStream;
            if (ms != null)
            {
                return ms.ToArray();
            }
            return null;
        }
    }
}
