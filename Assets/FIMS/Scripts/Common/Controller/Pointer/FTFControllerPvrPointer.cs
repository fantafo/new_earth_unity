﻿using FTF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FTFControllerPvrPointer : Pvr_UIPointer
{
    //public static Vector3 customDot;

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        //customDot = transform.position;
    }

    public override void OnUIPointerElementClick(UIPointerEventArgs e)
    {
        base.OnUIPointerElementClick(e);

        var target = ExecuteEvents.ExecuteHierarchy(e.raycastResult.gameObject, this.pointerEventData, ExecuteEvents.pointerDownHandler);
        if (target != null)
        {
            var coinPoint = target.GetComponent<CoinPoint>();
            if (coinPoint != null)
            {
                coinPoint.Finding(PlayerInstance.main.instanceId, target.name);
            }

            var coinPoint2 = target.GetComponent<CoinChanceItem>();
            if (coinPoint2 != null)
            {
                coinPoint2.OnClick();
            }
        }

    }

    protected override void ConfigureEventSystem()
    {
        //EventSystem가 두개 이상일때 문제발생. 수정
        if (!cachedEventSystem)
        {
            //cachedEventSystem = FindObjectOfType<EventSystem>();
            EventSystem[] events = FindObjectsOfType<EventSystem>();
            for (int i = 0; i < events.Length; ++i)
            {
                if (events[i].GetComponent<Pvr_InputModule>())
                {
                    cachedEventSystem = events[i];
                    break;
                }
            }
        }

        if (!cachedVRInputModule)
        {
            cachedVRInputModule = cachedEventSystem.GetComponent<Pvr_InputModule>();
        }

        if (cachedEventSystem && cachedVRInputModule)
        {
            if (pointerEventData == null)
            {
                pointerEventData = new PointerEventData(cachedEventSystem);
            }

            if (!cachedVRInputModule.pointers.Contains(this))
            {
                cachedVRInputModule.pointers.Add(this);
            }
        }
    }
}
