﻿using System;

namespace FTF.Packet
{
    public class C_Voice
    {
        public static byte[] Init(int freq, byte channel, int frameSample)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Voice_Initialized);
            w.WriteD(freq);
            w.WriteC(channel);
            w.WriteD(frameSample);
            return w.ToBytes();
        }
        public static byte[] Data(byte[] data)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Voice_Data);
            w.WriteH(data.Length);
            w.Write(data);
            return w.ToBytes();
        }
    }
}