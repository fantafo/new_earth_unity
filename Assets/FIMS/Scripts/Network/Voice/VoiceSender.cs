﻿using FTF.Packet;
using FTF.VoIP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace FTF
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(VoiceRecorder))]
    public class VoiceSender : SMonoBehaviour, IVoiceSender
    {
        VoiceRecorder recorder;
        bool sendInitializePacket;

        private void Awake()
        {
            recorder = GetComponent<VoiceRecorder>();
        }

        private void Update()
        {
            if (Networker.State < NetworkState.Validated)
                return;

            if(sendInitializePacket)
            {
                Networker.Send(C_Voice.Init(recorder._voiceInfo.SamplingRate, (byte)recorder._voiceInfo.Channels, recorder._voiceInfo.FrameDurationSamples));
            }
        }


        bool IVoiceSender.IsInitialized { get { return true; } }
        void IVoiceSender.RestartSender()
        {
            sendInitializePacket = true;
        }
        void IVoiceSender.StartSender()
        {
            sendInitializePacket = true;
        }
        void IVoiceSender.Broadcast(byte[] sendBuffer, int v, int length)
        {
            // 방에 들어가있을때만 목소리를 전송한다
            if (Networker.State >= NetworkState.Room)
            {
                Networker.Send(C_Voice.Data(sendBuffer));
            }
        }
    }
}
