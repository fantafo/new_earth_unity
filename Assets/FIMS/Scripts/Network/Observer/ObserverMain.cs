﻿using FTF.Packet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace FTF
{
    /// <summary>
    /// 옵저버의 MainPlayerController 의 역할을 하며, 옵저가 활성화 돼 있는 동안,
    /// 해당 MainPlayerController 외의 컨트롤러는 감춰져 있어야한다.
    /// 이 컨트롤러의 ObserverSchedule 코루틴으로 인해 옵저버의 행동이 재현되며,
    /// ObserverPacketHandler를 통해 원하는 데이터를 취득할 수 있다.
    /// </summary>
    public class ObserverMain : FTFPlayerController
    {
        public ObserverScheduler scheduler = new ObserverScheduler();
        public CanvasGroup messageGroup;
        public int changeNextPlayer = 0;
        private GameObject ObserverUI;
        private int lookPlayerNum = 0;


        protected override void Start()
        {
            base.Start();
            StartCoroutine(ObserverSchedule());

            // 이름 초기화
            var text = messageGroup.GetComponentInChildren<Canvas>().GetComponentInChildren<Text>();
            text.text = Application.productName;
        
    }

        protected override void UpdateModelTransform()
        {
            if (info.model != null)
            {
                cameraRig.transform.position = info.model.headContainer.position;
                cameraRig.transform.rotation = info.model.headContainer.rotation;
            }
        }

        IEnumerator ObserverSchedule()
        {
            ScreenFader.main.FadeOut(0);
            while (Networker.State < NetworkState.Validating)
                yield return null;

            Debug.Log("Observer/ Connected Server");

            Networker.main.send(C_Common.Version(Networker.main.clientIdentifier, Networker.main.clientPacketVersion, false));
            yield return scheduler.Wait(ServerOpcode.C_ServerVersion);

            Debug.Log("Observer/ Version Check");

            Networker.Send(C_Common.LoginObserver());
            yield return scheduler.Wait(ServerOpcode.C_LoginResult);

            Debug.Log("Observer/ Logined");

            STweenState animFader = null;
            while (true)
            {
                Debug.Log("Observer/ Request RoomList");
                // 방목록을 받으면 새로운 방으로 들어가게된다.
                Networker.Send(C_Channel.RequestList());
                yield return scheduler.Wait(ServerOpcode.Room_List);

                // 로딩완료를 기다린다
                bool isNewRoom = scheduler.RequestNext;
                if (isNewRoom)
                {
                    Debug.Log("Observer/ Found Next Room");
                    if (Networker.State == NetworkState.Playing)
                    {
                        Debug.Log("Observer/ Exit The Room");
                        animFader.TryStop();
                        animFader = STween.alpha(messageGroup, 1, 1).SetOnComplete(() => animFader = null);
                        Networker.Send(C_Room.Exit());
                        ScreenFader.main.FadeOut();

                        yield return scheduler.Wait(ServerOpcode.Scene_End, ServerOpcode.Room_Exit);
                        yield return Waits.Wait(2);

                    }

                    Debug.Log("Observer/ Join next Room");
                    Networker.Send(C_Channel.Join((long)scheduler.ResultValue, ConnectInfo.RoomPassword));
                    yield return scheduler.Wait(ServerOpcode.Room_Info);

                    // 시청할 첫번째 대상 지정
                    //Debug.Log("Observer/ Room Players in " + RoomInfo.main.players.Length);
                    //for (int i = 0; i < RoomInfo.main.players.Length; i++)
                    //{
                    //    if (RoomInfo.main.players[i] != null && RoomInfo.main.players[i].instance != null)
                    //    {
                    //        PlayerInstance.lookMain = RoomInfo.main.players[i].instance;
                    //        Debug.Log("Observer/ Select Player (" + i + "/" + PlayerInstance.lookMain.userName + ")");
                    //    }
                    //}

                    // 게임이 시작될 때까지 대기한다
                    while (Networker.State < NetworkState.Playing)
                        yield return null;
                    //yield return scheduler.Wait(ServerOpcode.Scene_Start);

                    animFader.TryStop();
                    animFader = STween.alpha(messageGroup, 0, 1).SetOnComplete(() => animFader = null);
                    ObserverUI = GameObject.Find("ObserverUI");

                    PlayerInstance.lookMain = RoomInfo.main.players[0].instance;
                    ObserverUI.SendMessage("ObserverUIUpdate", 0);
                }

                // 각 플레이어들을 돌면서 보여준다.




               

                //if (teach && RoomInfo.main.players.Length > 0 && RoomInfo.main.players[0].instance != null)
                //{
                //    print("Observer/ Select Teacher");
                //    PlayerInstance.lookMain = RoomInfo.main.players[0].instance;
                //}
                //else if (RoomInfo.main.players.Length > 1 && RoomInfo.main.players[1].instance != null)
                //{
                //    print("Observer/ Select Player 1");
                //    PlayerInstance.lookMain = RoomInfo.main.players[1].instance;
                //}
                yield return Waits.Wait(changeNextPlayer);

                // 새로운 방에 들어온 방이 아니라면 패킷 속도를 위해 1초정도 기다린다.
                if (isNewRoom == false)
                {
                    print("Observer/ Wait for 1 sec");
                    yield return Waits.Wait(1);
                }
            }
        }

        bool teach = true;

        protected override void Update()
        {
            base.Update();
            if (Input.GetKeyDown("`"))
            {
                teach = !teach;
            }
            if (Input.GetKeyDown(KeyCode.RightArrow) && lookPlayerNum < RoomInfo.main.maxUser && RoomInfo.main.players[lookPlayerNum+1] != null)
            {
                lookPlayerNum++;
                print("Observer/ Select Player " + lookPlayerNum);
                PlayerInstance.lookMain = RoomInfo.main.players[lookPlayerNum].instance;
                ObserverUI.SendMessage("ObserverUIUpdate", lookPlayerNum);
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow) && lookPlayerNum > 0 && RoomInfo.main.players[lookPlayerNum -1] != null)
            {
                lookPlayerNum--;
                print("Observer/ Select Player " + lookPlayerNum);
                PlayerInstance.lookMain = RoomInfo.main.players[lookPlayerNum].instance;
                ObserverUI.SendMessage("ObserverUIUpdate", lookPlayerNum);

            }

        }

    }

    public class ObserverScheduler
    {
        ServerOpcode[] waitCode;
        public bool IsComplete;
        public bool RequestNext;
        public object ResultValue { get; set; }

        public IEnumerator Wait(params ServerOpcode[] code)
        {
            waitCode = code;
            IsComplete = false;
            RequestNext = false;
            while (!IsComplete)
            {
                yield return null;
            }
        }
        public void Next(ServerOpcode code, object value = null)
        {
            ResultValue = value;
            foreach (var cd in waitCode)
            {
                if (cd == code)
                {
                    goto NEXT;
                }
            }
            return;

            NEXT:
            {
                IsComplete = true;
                RequestNext = true;
            }
        }
        public void Break(ServerOpcode code)
        {
            foreach (var cd in waitCode)
            {
                if (cd == code)
                {
                    goto NEXT;
                }
            }
            return;

            NEXT:
            {
                IsComplete = true;
                RequestNext = false;
            }
        }
    }
}
