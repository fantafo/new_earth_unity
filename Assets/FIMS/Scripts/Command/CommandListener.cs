﻿using FTF.Packet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 서버에서 보내온 Command를 알맞은 대상에게 보내주는 역할을 한다.
    /// Command패킷의 1byte에서 OPCODE를 가져온 뒤,
    /// 해당 옵코드에 해당하는 대상 클래스들에게 패킷을 전달해준다.
    /// </summary>
    public class CommandListener : SMonoBehaviour
    {
        //================================================================================================
        //
        //                                  Listener Collection
        //
        //================================================================================================

        static List<Action<BinaryReader>>[] listeners = new List<Action<BinaryReader>>[256];

        public static void AddListener(int opcode, Action<BinaryReader> listener)
        {
            if (listeners[opcode] == null)
                listeners[opcode] = new List<Action<BinaryReader>>();

            listeners[opcode].Add(listener);
        }

        public static void RemoveListener(int opcode, Action<BinaryReader> listener)
        {
            if (listeners[opcode] != null)
            {
                listeners[opcode].Remove(listener);
            }
        }


        //================================================================================================
        //
        //                                 Process Received Command
        //
        //================================================================================================

        // 임시 버퍼
        static byte[] buffer;
        static MemoryStream stream;
        static BinaryReader reader;

        /// <summary>
        /// PacketHandler에서 전달해준 패킷을 처리합니다.
        /// 처리된 데이터는 listeners목록을 대상으로 배포됩니다.
        /// </summary>
        public static void OnReceive(BinaryReader read)
        {
            // 버퍼 설정
            if(buffer == null)
            {
                buffer = new byte[4096];
                stream = new MemoryStream(buffer);
                reader = new BinaryReader(stream);
            }

            // 데이터 읽기
            int size = read.ReadH();
            read.Read(buffer, 0, size);

            // 데이터 전송
            int opcode = buffer[0];
            var ls = listeners[opcode];
            if (ls != null)
            {
                for (int i = 0; i<ls.Count; i++)
                {
                    stream.Position = 0;
                    ls[i](reader);
                }
            }
        }
    }
}
