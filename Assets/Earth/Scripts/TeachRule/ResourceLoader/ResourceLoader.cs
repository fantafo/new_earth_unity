﻿using FTF.Earth;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Video;
namespace FTF
{
    /// <summary>
    /// 리소스를 불러오고 삭제하는 메인 컴포넌트입니다.
    /// Earth에서는 360용 프리팹을 불러오는데 사용합니다.
    /// </summary>
    public class ResourceLoader : AutoCommander
    {
        public static ResourceLoader main;
        public FTFLineDrawer drawer;
        public TeachRuleController teachMenu;
        public DemoEarthController earth;
        public string currentLoadPath;
        public bool earthActive;
        GameObject createdObject;
        bool isLoading;
        private Coroutine ShowCor;
        //show()함수가 두번 호출되는 경우 있어서
        bool isShow = false;
        protected override void Awake()
        {
            main = this;
            base.Awake();
        }
        private void Update()
        {
            if (VRInput.BackButton && KeyStack.Is(this))
            {
                if (!isLoading)
                {
                    Broadcast("Hide");
                }
            }
        }
        /// <summary>
        /// Path에 해당하는 리소스를 생성합니다.
        /// 생성하는 과정에서 Fade In,Out이 일어납니다.
        /// 또한 이전에 생성된 리소스가 있을경우에는 모두 제거하게됩니다.
        /// </summary>
        /// <param name="path"></param>
        [OnCommand]
        public void Show(string path)
        {
            Debug.Log("path : " + path);
            if (!isShow || ShowCor == null)
            {
                isShow = true;
                ShowDirect(path, null);
            }
        }
        public void ShowDirect(string path, Action onLoadImmediateCallback)
        {
            SLog.Debug("ResourceLoader", "Show " + path);
            isLoading = true;
            if (path.Exists())
            {

                ShowCor = StartCoroutine(LoadStart(path, onLoadImmediateCallback));
            }
        }
        private IEnumerator LoadStart(string path, Action onLoadImmediateCallback)
        {
            KeyStack.Push(this);
            currentLoadPath = path;
            teachMenu.ctalogBtn.isOn = false;
            teachMenu.infoBtn.isOn = false;
            for (int i = 0; i < 5; i++)
            {
                // 로드 시작
                var goAsync = Resources.LoadAsync<GameObject>(path);
                //
                ScreenFader.main.FadeOut(0.4f);
                yield return Waits.Wait(0.4f);
                //blinder.gameObject.SetActive(true);
                //yield return STween.value(1f, 0f, 1f, v => blinder.material.SetFloat("_Cutoff", v)).Wait();
                // 화면이 완전히 가려지고 새로운 대상 생성
                drawer.ClearAllLine();
                Debug.Log("ResourceLoader check point 1");
                if (createdObject)
                {
                    Debug.Log("ResourceLoader check point 2");
                    var player = createdObject.GetComponentInChildren<VideoPlayer>();
                    Debug.Log("player is : " + player);
                    if (player != null)
                    {
                        player.Stop();
                        //player.targetMaterialRenderer.material.SetTexture(player.targetMaterialProperty, null);
                        //player.targetMaterialRenderer = null;
                        //player.targetTexture = null;
                        //try
                        //{
                        //    Resources.UnloadAsset(player.clip);
                        //    DestroyImmediate(player.clip);
                        //}catch(Exception e)
                        //{
                        //    e.PrintStackTrace();
                        //}
                        yield return null;
                        DestroyImmediate(player);
                        //yield return null;
                        //DestroyImmediate(player.gameObject);
                        //yield return null;
                    }
                    DestroyImmediate(createdObject);
                }
                Debug.Log("ResourceLoader check point 3");
                yield return goAsync;
                Debug.Log("ResourceLoader check point 4");
                yield return Waits.Wait(0.5f);
                Resources.UnloadUnusedAssets();
                System.GC.Collect(2, GCCollectionMode.Forced);
                yield return null;
                Debug.Log("ResourceLoader check point 5");
                
                while (!goAsync.isDone) {
                    yield return 0;
                }
                
                
                if (goAsync.asset)
                {
                    createdObject = Instantiate<GameObject>((GameObject)goAsync.asset);
                    isShow = false;
                    earth.gameObject.SetActive(false);
                    earthActive = false;
                    if (onLoadImmediateCallback != null)
                        onLoadImmediateCallback();
                }
                if (!isShow)
                {
                    SLog.Debug("Resource load Success");
                    break;
                }
            }
            if (isShow)
            {
                SLog.Debug("Resource load Failed");
                isShow = false;
            }
            // 화면을 다시 보여줌
            //yield return STween.value(0f, 1f, 1f, v => blinder.material.SetFloat("_Cutoff", v)).Wait();
            //blinder.gameObject.SetActive(false);
            Debug.Log("ResourceLoader check point 6");
            yield return Waits.Wait(0.6f);
            ScreenFader.main.FadeIn(0.4f);
            Debug.Log("ResourceLoader check point 7");
            isLoading = false;
        }
        [OnCommand]
        public void Hide()
        {
            isLoading = true;
            StartCoroutine(Hiding());
        }
        private IEnumerator Hiding()
        {
            currentLoadPath = null;
            // 화면 가리기
            //blinder.gameObject.SetActive(true);
            //yield return STween.value(1f, 0f, 1f, v => blinder.material.SetFloat("_Cutoff", v)).Wait();
            ScreenFader.main.FadeOut(0.4f);
            yield return Waits.Wait(0.4f);
            drawer.Broadcast("ClearAll");
            if (createdObject)
                DestroyImmediate(createdObject);
            createdObject = null;
            Resources.UnloadUnusedAssets();
            // 화면을 다시 보여줌
            //yield return STween.value(0f, 1f, 1f, v => blinder.material.SetFloat("_Cutoff", v)).Wait();
            //blinder.gameObject.SetActive(false);
            ScreenFader.main.FadeIn(1);
            yield return Waits.Wait(1);
            SoundManager.INS.PlayBGSound(SOUND_BG_ID.MAIN_BG);
            earth.gameObject.SetActive(true);
            earthActive = true;
            KeyStack.Pop(this);
            isLoading = false;
        }
        internal void Clear()
        {
            currentLoadPath = null;
            isLoading = false;
            if (createdObject)
                DestroyImmediate(createdObject);
            createdObject = null;
            Resources.UnloadUnusedAssets();
        }
    }
}