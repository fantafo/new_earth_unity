﻿using FTF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

namespace FTF.Earth
{
    /// <summary>
    /// 카탈로그 메인컴포넌트로
    /// 애니메이션이나, 선택및 투표에 관한 컴포넌트입니다.
    /// </summary>
    public class Catalog : AutoCommander
    {
        public Animator animator;
        public CatalogItem[] items;
        public int voteTarget; // 내가 몇번째를 추천했는지 나타냅니다.

        private Coroutine voteDataSender;
        protected override void Awake()
        {
            base.Awake();

            items.SetGameObectActive(false);
            foreach (var it in items)
            {
                if (it)
                {
                    it.catalog = this;
                }
            }

            RenderSettings.skybox.SetFloat("_value", 0);
            Resources.UnloadUnusedAssets();
        }


        /////////////////////////////////////////////////////////////////////////////////////////
        //
        // ActiveForCountry에서 SendMessage로 보내준다.
        //
        /////////////////////////////////////////////////////////////////////////////////////////
        public void Show(ActiveArgs delay)
        {
            SetVisible(true);
           
            if (PlayerInstance.lookMain.IsRoomMaster)
            {
                if (voteDataSender != null)
                {
                    StopCoroutine(voteDataSender);
                }
                voteDataSender = StartCoroutine("VoteUpdateSender");
            }
        }
        public void Hide(ActiveArgs args)
        {
            SetVisible(false);
            if (PlayerInstance.lookMain.IsRoomMaster)
            {
                if (voteDataSender != null)
                {
                    StopCoroutine(voteDataSender);
                }
            }
        }

        STweenState state;
        void SetVisible(bool active)
        {
            state.TryStop();
            state = null;

            if (active)
            {
                voteTarget = -1;
                animator.SetBool("Open", true);
                foreach (var item in items)
                {
                    if (item != null && item.marker)
                    {
                        item.marker.SetActive(false);
                    }
                }
            }
            else
            {
                animator.SetBool("Open", false);
                state = STween.delayCall(1, () => { gameObject.SetActive(false); state = null; });
            }
            Resources.UnloadUnusedAssets();
        }

        /////////////////////////////////////////////////////////////////////////////////////////
        //
        // Commands
        //
        /////////////////////////////////////////////////////////////////////////////////////////
        [OnCommand]
        public void VoteItem(string name, int index)
        {
            if (name == this.name)
            {
                Debug.Log("VoteItem " + index + " /" + items.Length);
                items[index].Vote++;
                Debug.Log("VoteItemComplete");
            }
        }

        [OnCommand]
        public void UnvoteItem(string name, int index)
        {
            if (name == this.name)
            {
                Debug.Log("UnvoiteItem " + index + " /" + items.Length);
                items[index].Vote--;
                Debug.Log("UnvoteItemComplete");
            }
        }


        [OnCommand]
        public void VoteUpdate(int one = 0, int two = 0, int three = 0, int four = 0)
        {
            if (PlayerInstance.lookMain.IsRoomMaster)
                return;

            if (items.Length >= 1)
                items[0].Vote = one;
            if (items.Length >= 2)
                items[1].Vote = two;
            if (items.Length >= 3)
                items[2].Vote = three;
            if (items.Length >= 4)
                items[3].Vote = four;
        }

        IEnumerator VoteUpdateSender()
        {
            while (true)
            {
                if (items.Length == 1)
                    Broadcast("VoteUpdate", items[0].Vote, 0, 0, 0);
                else if (items.Length ==2)
                    Broadcast("VoteUpdate", items[0].Vote, items[1].Vote, 0, 0);
                else if (items.Length == 3)
                    Broadcast("VoteUpdate", items[0].Vote, items[1].Vote, items[2].Vote, 0);
                else 
                    Broadcast("VoteUpdate", items[0].Vote, items[1].Vote, items[2].Vote, items[3].Vote);

                //Debug.Log("VoteUpdateSender" + items[0].Vote + " " + items[1].Vote );
                yield return new WaitForSeconds(1.0f);
            }
            yield return 0;
        }
    }
}