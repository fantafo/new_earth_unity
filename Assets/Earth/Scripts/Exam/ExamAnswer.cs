﻿using FTF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExamAnswer : SMonoBehaviour {

    public Toggle toggle;
    public Text counter;
    public GameObject rightAnswer;
    public Text msg;
    public GameObject activeSuccess;
    public GameObject activeFailed;

}
